//
//  WeatherAPI.swift
//  WeatherApp
//
//  Created by devibharat on 06/04/21.
//

import Foundation

extension NetworkService {

func getWeather(lat: Double, long: Double, onSuccess: @escaping (Result) -> Void, onError: @escaping (String) -> Void) {
    guard let url = URL(string:APIConstants.buildURL(lat: lat, long: long)) else {
        onError("Error building URL")
        return
    }

    
    self.request(url: url) { (data) in
        do {
        let items = try JSONDecoder().decode(Result.self, from: data)
        onSuccess(items)
        } catch {
            onError(error.localizedDescription)
        }
        
    } onError: { (message) in
        onError(message)

    }
}
    
    func getForecast(lat: Double, long: Double, onSuccess: @escaping (ForecastModel) -> Void, onError: @escaping (String) -> Void) {
        guard let url = URL(string:APIConstants.buildURL(type: .forecast, lat: lat, long: long)) else {
            onError("Error building URL")
            return
        }

        
        self.request(url: url) { (data) in
            do {
            let items = try JSONDecoder().decode(ForecastModel.self, from: data)
            onSuccess(items)
            } catch {
                onError(error.localizedDescription)
            }
            
        } onError: { (message) in
            onError(message)

        }
    }
    
    
    func getForecastDays(lat: Double, long: Double, onSuccess: @escaping ([ForecastViewModel]) -> Void, onError: @escaping (String) -> Void) {

        self.getForecast(lat: lat, long: long) { (data) in
            var list:[ForecastViewModel] = []
            
            let result = data.list?.chunked(into: 24/3 )

            result?.forEach { item in
                let arr = item.sorted { $0.main?.temp ?? 0.0 > $1.main?.temp ?? 0.0 }
                let max =  arr.first?.main?.temp
                
                print("\(max ?? 0.0)")
                list.append(ForecastViewModel(data: arr.first))
                
                
                
                
             //   Text("\(Int(result?.main?.temp?.rounded() ?? 0.0))°F")

            
            }
            
            onSuccess(list)
            
            
        } onError: { (error) in
            onError(error)

        }

    }

}

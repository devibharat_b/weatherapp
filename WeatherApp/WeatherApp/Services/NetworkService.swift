//
//  File.swift
//  WeatherApp
//
//  Created by devibharat on 04/04/21.
//

import Foundation


class NetworkService {
    static let shared = NetworkService()
    
 
    let session = URLSession(configuration: .default)
    

    
    func request(url:URL, onSuccess: @escaping (Data) -> Void, onError: @escaping (String) -> Void) {
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            
            print("Request URL: ",url.absoluteString)

            DispatchQueue.main.async {
                if let error = error {
                    onError(error.localizedDescription)
                    return
                }
                
                guard let data = data, let response = response as? HTTPURLResponse else {
                    onError("Invalid data or response")
                    return
                }
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary

                    print("Response URL: ", json ?? "")
                    if response.statusCode == 200 {
                        onSuccess(data)
                    } else {
                        onError("Response wasn't 200. It was: " + "\n\(response.statusCode)")
                    }
                } catch {
                    onError(error.localizedDescription)
                }
            }
            
        }
        task.resume()
    }
    
    /*
  
    func getWeather(lat: Double,long: Double, onSuccess: @escaping (Result) -> Void, onError: @escaping (String) -> Void) {
        
        guard let url = URL(string:APIConstants.buildURL(lat: lat, long: long)) else {
            onError("Error building URL")
            return
        }
        let task = session.dataTask(with: url) { (data, response, error) in
            
            
            print("Request URL: ",url.absoluteString)

            DispatchQueue.main.async {
                if let error = error {
                    onError(error.localizedDescription)
                    return
                }
                
                guard let data = data, let response = response as? HTTPURLResponse else {
                    onError("Invalid data or response")
                    return
                }
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary

                    print("Response URL: ", json ?? "")
                    if response.statusCode == 200 {
                        let items = try JSONDecoder().decode(Result.self, from: data)
                        onSuccess(items)
                    } else {
                        onError("Response wasn't 200. It was: " + "\n\(response.statusCode)")
                    }
                } catch {
                    onError(error.localizedDescription)
                }
            }
            
        }
        task.resume()
    }
 */
    
}



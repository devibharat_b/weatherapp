//
//  APIConstants.swift
//  WeatherApp
//
//  Created by devibharat on 05/04/21.
//

import Foundation


struct APIConstants {
    
    static  let baseURL = "https://api.openweathermap.org/data/2.5"
    static let APIkey = "fae7190d7e6433ec3a45285ffcf55c86"
    
    static func buildURL(type: WeatherType = .weather, lat: Double,long: Double) -> String {
        let endPoint = "/\(type.rawValue)?lat=" + "\(lat)" + "&lon=" + "\(long)" + "&units=\(GlobalUser.units.rawValue)" + "&appid=" + APIkey
        return baseURL + endPoint
    }
    
    static  func buildImageURL(name:String) -> String {
        return "http://openweathermap.org/img/wn/\(name)@2x.png"
    }
    
    enum WeatherType: String {
        case weather, forecast
    }
}

//
//  LocationProvider.swift
//
//  Created by DeviBharat on 27.06.20.
//  Copyright © 2020 DeviBharat. All rights reserved.
//

import Foundation
import CoreLocation
import Combine
import MapKit

public class LocationProvider: NSObject, ObservableObject {
    
    private let lm = CLLocationManager()
    public let locationWillChange = PassthroughSubject<CLLocation, Never>()
    
    @Published public private(set) var location: CLLocation? {
        willSet {
            locationWillChange.send(newValue ?? CLLocation())
        }
    }
    @Published public var authorizationStatus: CLAuthorizationStatus?
    public var onAuthorizationStatusDenied : () -> Void = {presentLocationSettingsAlert()}
    
    private let geocoder = CLGeocoder()
      
      @Published var placemark: CLPlacemark? {
        willSet { objectWillChange.send() }
      }
        
    public var callback: (LocationModel) -> Void
    
      private func geocode() {
      guard let location = self.location else { return }
      geocoder.reverseGeocodeLocation(location, completionHandler: { (places, error) in
        
        print(#function)
        if error == nil {
            self.placemark = places?.first
            
            let location =  LocationModel(location: self.placemark)

            self.callback(location)
            
            self.stop()
            
        } else {
          self.placemark = nil
        }
      })
    }
    
    public override init() {
        
        self.callback = { data in
              print(data)
          }
        
        super.init()

        self.lm.delegate = self
        self.lm.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    public func requestAuthorization() -> Void {
        if self.authorizationStatus == CLAuthorizationStatus.denied {
            onAuthorizationStatusDenied()
        }
        else {
            self.lm.requestWhenInUseAuthorization()
            self.lm.startUpdatingLocation()

        }
    }
    
    // Start the Location Provider.
    public func start() throws -> Void {
        self.requestAuthorization()
        
        if let status = self.authorizationStatus {
            guard status == .authorizedWhenInUse || status == .authorizedAlways else {
                throw LocationProviderError.noAuthorization
            }
        }
        else {
            print(#function, "No location authorization status set by delegate yet. Try to start updates anyhow.")
        }
        self.lm.startUpdatingLocation()
    }
    
    // Stop the Location Provider.
    public func stop() -> Void {
        self.lm.stopUpdatingLocation()
    }
    
    func geocode(_ lat: Double, long: Double) {

        
        let location = CLLocation(latitude: lat, longitude: long)

       geocoder.reverseGeocodeLocation(location) { (placemarks, error) in

        if let error = error {
            print("Unable to Reverse Geocode Location (\(error))")

        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                
                let location =  LocationModel(location: placemark)

                self.callback(location)

            } else {
                print("No Matching Addresses Found")
            }
        }
        
       }

     
    }
    

    
}

/// Present an alert that suggests to go to the app settings screen.
public func presentLocationSettingsAlert(alertText : String? = nil) -> Void {
    let alertController = UIAlertController (title: "Enable Location Access", message: alertText ?? "Enable location access in the application settings. Go to Settings now?", preferredStyle: .alert)
    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
        guard let settingsUrl = URL(string:UIApplication.openSettingsURLString) else {
            return
        }
        UIApplication.shared.open(settingsUrl)
    }
    alertController.addAction(settingsAction)
    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
    alertController.addAction(cancelAction)
    UIApplication.shared.windows[0].rootViewController?.present(alertController, animated: true, completion: nil)
}


public enum LocationProviderError: Error {
    case noAuthorization
}



extension LocationProvider: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.authorizationStatus = status
        print(#function, status.name)
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        self.location = location
        self.geocode()
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let clErr = error as? CLError {
            switch clErr {
            case CLError.denied : do {
                print(#function, "Location access denied by user.")
                self.stop()
                self.requestAuthorization()
            }
            case CLError.locationUnknown : print(#function, "Location manager is unable to retrieve a location.")
            default: print(#function, "Location manager failed with unknown CoreLocation error.")
            }
        }
        else {
            print(#function, "Location manager failed with unknown error", error.localizedDescription)
        }
    }
}

extension CLAuthorizationStatus {
    var name: String {
        switch self {
        case .notDetermined: return "notDetermined"
        case .authorizedWhenInUse: return "authorizedWhenInUse"
        case .authorizedAlways: return "authorizedAlways"
        case .restricted: return "restricted"
        case .denied: return "denied"
        default: return "unknown"
        }
    }
}








public struct LocationModel {
    public var name: String?
    public var locality: String?
    public var sublocality: String?
    public var area: String?
    public var subArea: String?
    public var country: String?
    public var zip: String?
    public var latitude: Double?
    public var longitude: Double?
    public var address: String?

    init(location: MKPlacemark) {
        self.name = location.name
        self.locality = location.locality
        self.sublocality = location.subLocality
        self.area = location.administrativeArea
        self.subArea = location.subAdministrativeArea
        self.country = location.country
        self.zip = location.postalCode
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        self.address = getLocationAddress(data: location)

    }

    init(location: CLPlacemark?) {
        self.name = location?.name
        self.locality = location?.locality
        self.sublocality = location?.subLocality
        self.area = location?.administrativeArea
        self.country = location?.country
        self.zip = location?.postalCode
        self.latitude = location?.location?.coordinate.latitude
        self.longitude = location?.location?.coordinate.longitude
        if let loc = location as? MKPlacemark {
        self.address = getLocationAddress(data: loc)
        }

       }




    func getLocationAddress(data:MKPlacemark) -> String {

        var arr = [String]()

        if let value = data.subThoroughfare {
            if value != "" {
                arr.append(value)
            }
        }


        if let value = data.thoroughfare {
            if value != "" {
                arr.append(value)
            }
        }

        if let value = data.locality {
            if value != "" {
                arr.append(value)
            }
        }

        if let value = data.subLocality {
            if value != "" {
                arr.append(value)
            }
        }

        if let value = data.administrativeArea {
            if value != "" {
                arr.append(value)
            }
        }

        if let value = data.postalCode {
            if value != "" {
                arr.append(value)
            }
        }

        if let value = data.country {
            if value != "" {
                arr.append(value)
            }
        }

        var result = arr.joined(separator: ", ")

        if result == ", " || result == "" || result.isEmpty {
            result = "No Location"
        }
        return result
    }



}

//
//  AppConstants.swift
//  WeatherApp
//
//  Created by devibharat on 05/04/21.
//

import Foundation

let defaults = UserDefaults.standard

 struct GlobalUser {
    
    static var units: Units {

        if let value = defaults.value(forKey: "units") as? String {
            return Units(rawValue: value) ?? Units.imperial
        } else {
            return Units.imperial
        }
    }
    
    static var unitsText: String {

        if let value = defaults.value(forKey: "units") as? String {
            
            if Units(rawValue: value) == Units.metric {
                
                return "°C"
            } else {
                return "°F"

            }
            
        } else {
            return "°F"
        }
    }
    
     static var latitude: Double {
        get { return defaults.double(forKey: "latitude") }
        set { _ = newValue }
    }

      static var longitude: Double {
        get { return defaults.double(forKey: "longitude") }
        set { _ = newValue }
    }

}

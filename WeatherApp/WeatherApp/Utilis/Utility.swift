//
//  Utility.swift
//  WeatherApp
//
//  Created by devibharat on 05/04/21.
//

import Foundation


enum Units: String {
    case metric, imperial
}


 
extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}


extension Date {

static func getDayOfWeekFrom(date: Date) -> String {
    
    let dateFormatter = DateFormatter()
    dateFormatter.timeStyle = .none
    dateFormatter.dateStyle = .long
    var string = dateFormatter.string(from: date)
    if let index = string.firstIndex(of: ",") {
        string = String(string.prefix(upTo: index))
        return string
    }
    return "error"
}
}

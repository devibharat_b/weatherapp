//
//  ContentView.swift
//  WeatherApp
//
//  Created by devibharat on 04/04/21.
//

import SwiftUI
import CoreData

struct HomeView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.colorScheme) var colorScheme
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Item.timestamp, ascending: true)],
        animation: .default)
    private var items: FetchedResults<Item>
    @State private var showMap = false
    @State private var showSettings = false

    @ObservedObject var locationProvider = LocationProvider()

    @State var reloadToken = UUID()

    var body: some View {
        NavigationView {
            List {
                ForEach(items) { item in
                    CardView(item: item)
                }
                .onDelete(perform: deleteItems)
            }.id(reloadToken)
            .onAppear(perform: {
                
                self.getLocation { succuss, location  in
                    if succuss {
                    defaults.set(location.latitude ?? 0.0, forKey: "latitude")
                    defaults.set(location.longitude ?? 0.0, forKey: "longitude")
                    }
                }
                
            })
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    EditButton()
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    HStack {
                    Button(action: { showMap.toggle()} ) {
                        Image(systemName: "plus.circle.fill")
                            .foregroundColor(.blue)
                    }
                    .fullScreenCover(isPresented: self.$showMap){
                        MapView { lat, long  in
                            addItem(lat: lat, long: long)
                            
                            print("added : ","\(lat ),\(long)")

                            
                        }
                        .modifier(DarkModeViewModifier())

                    }
                            Button(action: { showSettings = true} ) {
                                Image(systemName: "gear")
                                    .foregroundColor(.blue)
                            }
                            .fullScreenCover(isPresented: self.$showSettings) {
                                
                                SettingsView(callback: { (true) in
                                    reloadToken = UUID()
                                    showSettings = false
                                })
                                    .modifier(DarkModeViewModifier())

                            }
                    }
                }
            }
            .navigationTitle("Home")
        }
    }
    
    private func addItem(lat:Double, long:Double) {
        withAnimation {
            let newItem = Item(context: viewContext)
            newItem.timestamp = Date()
            newItem.latitude = lat
            newItem.longitude = long
            
            do {
                try viewContext.save()
            } catch {
                
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
    
    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { items[$0] }.forEach(viewContext.delete)
            
            do {
                try viewContext.save()
            } catch {
                
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
    
    func getLocation(completion: @escaping (_ succus: Bool, _ data: LocationModel) -> Void) {

        do { try self.locationProvider.start()

        } catch {
            print("No location access.")
            self.locationProvider.requestAuthorization()
        }

        self.locationProvider.callback = { loc in
            print(loc)
            completion(true, loc)
        }
    }
}

private let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .medium
    return formatter
}()

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}


struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(item: Item())
    }
}


struct CardView: View {
    
    @Environment(\.colorScheme) var colorScheme

    
     var item: Item

   @State var result: Result?

    
    var body: some View {
        ZStack {
            colorScheme == .dark ? Color.black.cornerRadius(12) : Color.white.cornerRadius(12)
            HStack {
             
                VStack {

                    ImageWithURL(APIConstants.buildImageURL(name: result?.weather?.first?.icon ?? ""))
                    .frame(width: 70, height: 70)
                    Text("\(result?.weather?.first?.description ?? "")")

                }
               // .padding(.leading, 10)
                .padding()

                Spacer()

                
                VStack(alignment: .trailing, spacing: 10.0) {

                
                Text("\(result?.name ?? "")".uppercased())
                    .font(.title2).bold()
                    Text("\(Int(result?.main?.temp?.rounded() ?? 0.0))\(GlobalUser.unitsText)")

                }
               // .padding(.trailing, 10)
                .padding()

            }
            NavigationLink(destination: CityView(result: self.result)) {
                                    EmptyView()
                                }
                                .buttonStyle(PlainButtonStyle())
            .padding(.trailing, -32.0)

        }
        
        .background(Rectangle()
                        .fill(colorScheme == .dark ? Color.black : Color.white)
                        .cornerRadius(12)
                        .shadow(color: Color.secondary.opacity(0.24), radius: 8, x: 0, y: 4)
            )
        .frame( height: 140)
        .padding(.vertical, 5)
        .padding(.horizontal, 10)
        .cornerRadius(12)
        .onAppear(perform: {
            getWeather(lat: item.latitude, long: item.longitude)
        })
        
        
    }
    
    private func getWeather(lat:Double,long:Double) {
        
        NetworkService.shared.getWeather(lat: lat, long: long) { result in
            
            print(lat,",",long)
            self.result = result
            
        } onError: { (error) in
            print(error)
        }

    }

}

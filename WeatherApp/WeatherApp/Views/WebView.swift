//
//  WebView.swift
//  WeatherApp
//
//  Created by devibharat on 06/04/21.
//

import SwiftUI
import WebKit

struct WebView: UIViewRepresentable {
    var pageURL: String
    
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.load(pageURL)
    }
}

struct WebKitView: View {
var url: String
var title: String

var body: some View {
    WebView(pageURL: url)
    .edgesIgnoringSafeArea(.bottom)
        .navigationBarTitle("\(title.uppercased())", displayMode: .inline)

    }
}

struct WebKitView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            WebKitView(url: "https://apple.com", title: "Apple")
            .navigationBarTitle("Settings", displayMode: .inline)

        }
    }
}

extension WKWebView {
    func load(_ urlString: String) {
        if let url = URL(string: urlString) {
            print(urlString)
            let request = URLRequest(url: url)
            load(request)
        }
    }
}

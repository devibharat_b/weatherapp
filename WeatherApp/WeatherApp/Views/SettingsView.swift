//
//  File.swift
//  WeatherApp
//
//  Created by devibharat on 06/04/21.
//

import SwiftUI

struct SettingsView: View {
    
    @Environment(\.presentationMode) var presentationMode

    @AppStorage("isDarkMode") var isDarkMode: Bool = true

    private func resourceUrl(forFileName fileName: String) -> URL? {
        if let resourceUrl = Bundle.main.url(forResource: fileName,
                                             withExtension: "pdf") {
            return resourceUrl
        }
        return nil
    }
    
    @State private var selected = 0

    var callback: ((_ reload: Bool) -> Void)?

    var body: some View {
        NavigationView {
            
            Form {
                    Toggle(isOn: $isDarkMode) {
                                    Text("Darkmode")
                                        }
                   
                
                Section {
                    Text("Choose Units")

                           Picker(selection: $selected, label: Text("Choose Units")) {
                               Text("Imperial").tag(0)
                            Text("Metric").tag(1)

                           }
                           .pickerStyle(SegmentedPickerStyle())
                           .onChange(of: selected) { _ in
                                       print(selected)
                            
                            if selected == 0 {
                                defaults.set("imperial", forKey: "units")
                                print(GlobalUser.units.rawValue)

                            } else {
                                defaults.set("metric", forKey: "units")
                                print(GlobalUser.units.rawValue)

                            }

                                   }
                           .onAppear(perform: {
                            
                            print(GlobalUser.units.rawValue)

                            if GlobalUser.units == .imperial {
                                selected = 0
                            } else {
                                selected = 1

                            }
                            
                           })
                    

                }
                    
                NavigationLink(destination: WebKitView(url: resourceUrl(forFileName: "Weather")?.absoluteString ?? "", title: "How To Use")) {
                    Text("How To Use")
                }
            }

            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button(action: {
                       
                        dismiss()

                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2, execute: {
                        callback?(true)
                        })
                        
                    } ) {
                        Text("Close")

                    }
                }

            }
            .navigationTitle("Settings")
        }
    }
    
    private func dismiss() {
        self.presentationMode.wrappedValue.dismiss()
    }
}

public struct DarkModeViewModifier: ViewModifier {
@AppStorage("isDarkMode") var isDarkMode: Bool = true
public func body(content: Content) -> some View {
    content
        .environment(\.colorScheme, isDarkMode ? .dark : .light)
        .preferredColorScheme(isDarkMode ? .dark : .light)
    }
}

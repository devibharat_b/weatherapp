//
//  MapView.swift
//  WeatherApp
//
//  Created by devibharat on 04/04/21.
//


import MapKit

import SwiftUI
struct MapView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @State private var locations: [Mark] = []
    @State private var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(
            latitude: GlobalUser.latitude,
            longitude: GlobalUser.longitude
        ),
        span: MKCoordinateSpan(
            latitudeDelta: 10,
            longitudeDelta: 10
        )
    )
    
    var callback: ((_ lat: Double, _ long: Double) -> Void)

    @ObservedObject var locationProvider = LocationProvider()

    var body: some View {
        ZStack {
            Map(coordinateRegion: $region, annotationItems: locations) { location in
                MapAnnotation(
                    coordinate: location.coordinate,
                    anchorPoint: CGPoint(x: 0.5, y: 0.7)
                ) {
                    VStack{
                        if location.show {
                            Text("Test")
                        }
                        Image(systemName: "mappin.circle.fill")
                            .font(.title)
                            .foregroundColor(.red)
                            .onTapGesture {
                                let index: Int = locations.firstIndex(where: {$0.id == location.id})!
                                locations[index].show.toggle()
                            }
                    }
                }
            }
            
            Image(systemName: "mappin.circle.fill")
                .font(.title)
                .foregroundColor(.red)
            VStack {
                
                HStack {
                    Button(action: { dismiss()}) {
                        Image(systemName: "xmark.circle.fill")
                    }
                    .padding()
                    .foregroundColor(.primary)
                    .font(.title)
                    .padding(.top, 35)
                    Spacer()

                }
                
                Spacer()
                HStack {
                    Spacer()
                    Button(action:addLocation) {
                        Image(systemName: "plus")
                    }
                    .padding()
                    .background(Color.black.opacity(0.7))
                    .foregroundColor(.white)
                    .font(.title)
                    .clipShape(Circle())
                    .padding(20)
                }
            }
        }
        .onAppear(perform: {
        })
        .ignoresSafeArea()
    }
    
    
    func addLocation() {
        
        let coordinate = Mark(coordinate: region.center).coordinate
        dismiss()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
            
            locationProvider.geocode(coordinate.latitude, long: coordinate.longitude)
            self.locationProvider.callback = { loc in
                print(loc)
                
                callback(coordinate.latitude, coordinate.longitude)

                print("added : ","\(coordinate.latitude ),\(coordinate.longitude)")

            }


        })
        print(Mark(coordinate: region.center).coordinate.latitude,",",Mark(coordinate: region.center).coordinate.longitude)
    }
    
    
    private func dismiss() {
        self.presentationMode.wrappedValue.dismiss()
    }
}
struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView { _,_  in
            
        }
    }
}
struct Mark: Identifiable {
    let id = UUID()
    let coordinate: CLLocationCoordinate2D
    var show = false
}

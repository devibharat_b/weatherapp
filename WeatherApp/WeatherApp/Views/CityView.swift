//
//  CityView.swift
//  WeatherApp
//
//  Created by devibharat on 05/04/21.
//

import SwiftUI

struct CityView: View {
    
    var result: Result?
    @State var data: [ForecastViewModel]?
    
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        VStack {
            ZStack {
                colorScheme == .dark ? Color.black.cornerRadius(12) : Color.white.cornerRadius(12)
                
                VStack {
                    
                    Text("\(result?.name ?? "")".uppercased())
                        .font(.largeTitle).bold()
                    
                    HStack(alignment: .center) {
                        
                        VStack {
                            
                            ImageWithURL(APIConstants.buildImageURL(name: result?.weather?.first?.icon ?? ""))
                                .frame(width: 70, height: 70)
                            Text("\(result?.weather?.first?.description ?? "")")
                                .font(.title2).bold()
                                .foregroundColor(.secondary)
                            
                            
                        }
                        // .padding(.leading, 10)
                        .padding()
                        
                        Spacer()
                        
                        
                        
                        
                        
                        Text("\(Int(result?.main?.temp?.rounded() ?? 0.0))\(GlobalUser.unitsText)")
                            .font(.largeTitle).bold()
                            .foregroundColor(.secondary)
                            
                            
                            
                            
                            .padding()
                        
                    }
                    
                    DetailView(result: self.result)
                    //  .padding()
                    
                    Spacer()
                }
                .padding()
                
            }
            
            .background(Rectangle()
                            .fill(colorScheme == .dark ? Color.black : Color.white)
                            .cornerRadius(12)
                            .shadow(color: Color.secondary.opacity(0.24), radius: 8, x: 0, y: 4)
            )
            
            .padding(.vertical, 5)
            .padding(.horizontal, 10)
            .cornerRadius(12)
            
            
            ForcastView(viewModel: data)
            Spacer()
            
            
        }
        .padding()
        .onAppear(perform: {
            
            getForecast(lat: result?.coord?.lat ?? 0.0, long: result?.coord?.lon ?? 0.0)
            
        })
        
        
    }
    
    
    private func getForecast(lat:Double,long:Double) {
        
        NetworkService.shared.getForecastDays(lat: lat, long: long) { result in
            
            print(lat,",",long)
            self.data = result
            
        } onError: { (error) in
            print(error)
        }
        
    }
    
}

struct CityView_Previews: PreviewProvider {
    static var previews: some View {
        CityView()
    }
}


struct ForcastView: View {
    
    var viewModel: [ForecastViewModel]?
    
    var body: some View {
        
        HStack(spacing: 8) {
            if let data = viewModel {
                ForEach(Array(data.enumerated()), id: \.offset) { index, item in
                    
                    VStack {
                        
                        Text("\(item.day)")                        
                        ImageWithURL(APIConstants.buildImageURL(name: item.icon))
                            .frame(width: 50, height: 50)
                        
                        Text("\(item.temp)")
                    }
                }
            }
        }
    }
}


struct DetailView: View {
    
    var result: Result?
    
    var body: some View {
        
        HStack {
            VStack (alignment: .leading) {
                HStack {
                    
                    
                    Image(systemName: "cloud.fill")
                        .foregroundColor(.secondary)
                        .font(.system(size: 30))
                        .frame(width: 50, height: 50)
                    
                    
                    Text("\(result?.clouds?.all ?? 0)% Cloudy")
                        .foregroundColor(.gray)
                        .font(.system(.callout, design: .rounded))
                }
                
                
                HStack {
                    
                    
                    Image(systemName: "wind")
                        .foregroundColor(.secondary)
                        .font(.system(size: 30))
                        .frame(width: 50, height: 50)
                    
                    Text("\(String(format: "%.1f", result?.wind?.speed ?? 0)) M/s Wind Speed")
                        .foregroundColor(.gray)
                        .font(.system(.callout, design: .rounded))
                }
                
                
                HStack {
                    
                    Image(systemName: "thermometer.sun.fill")
                        .foregroundColor(.secondary)
                        .font(.system(size: 30))
                        .frame(width: 50, height: 50)
                    
                    
                    Text("\(result?.main?.humidity ?? 0)% Humidity")
                        .foregroundColor(.gray)
                        .font(.system(.callout, design: .rounded))
                    
                }
                
            }
            .padding(.horizontal)
            .padding(.bottom)
            
            Spacer()
            
            HStack {
                LinearGradient(gradient: Gradient(colors: [.green, .orange]), startPoint: .top, endPoint: .bottom)
                    .mask(Image(systemName: "thermometer")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                    ).frame(width: 30, height: 60, alignment: .center)
                
                VStack (spacing: 14){
                    
                    Text("\(Int(result?.main?.tempMax?.rounded() ?? 0.0))\(GlobalUser.unitsText)")
                        .font(.system(.body, design: .rounded))
                        .fontWeight(.medium)
                        .foregroundColor(.orange)
                    
                    Text("\(Int(result?.main?.tempMin?.rounded() ?? 0.0))\(GlobalUser.unitsText)")
                        .font(.system(.body, design: .rounded))
                        .fontWeight(.medium)
                        
                        .foregroundColor(.green)
                    
                }
            }
            
        }
        
    }
}


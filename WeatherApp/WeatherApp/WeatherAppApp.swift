//
//  WeatherAppApp.swift
//  WeatherApp
//
//  Created by devibharat on 04/04/21.
//

import SwiftUI

@main
struct WeatherAppApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            HomeView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .modifier(DarkModeViewModifier())

        }
    }
}

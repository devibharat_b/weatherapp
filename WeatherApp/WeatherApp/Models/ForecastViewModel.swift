//
//  File.swift
//  WeatherApp
//
//  Created by devibharat on 06/04/21.
//

import Foundation

struct ForecastViewModel:Identifiable {
    let id = UUID()
    let day: String
    let icon: String
    let temp: String
    
    init(data: ForecastList?) {
        let date = Date(timeIntervalSince1970: Double(data?.dt ?? 0))
        let dayString = Date.getDayOfWeekFrom(date: date)
        self.day = dayString
        self.icon = data?.weather?.first?.icon ?? ""
        self.temp = "\(Int(data?.main?.temp ?? 0.0))\(GlobalUser.unitsText)"

    }

}

//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by devibharat on 04/04/21.
//

import Foundation


// MARK: - Result
struct Result: Codable {
    let coord: Coord?
    let weather: [Weather]?
    let base: String?
    let main: Main?
    let visibility: Int?
    let wind: Wind?
    let clouds: Clouds?
    let dt: Int?
    let sys: Sys?
    let timezone, id: Int?
    let name: String?
    let cod: Int?
    
}


// MARK: - Clouds
struct Clouds: Codable {
    let all : Int?
}

// MARK: - Coord
struct Coord: Codable {
    let lon, lat: Double?
}

struct Main : Codable {
    let temp : Double?
    let feelsLike : Double?
    let tempMin : Double?
    let tempMax : Double?
    let pressure : Int?
    let humidity : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case temp  //=  //"temp"
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure  //=  "pressure"
        case humidity  //=  "humidity"
    }
}

// MARK: - Sys
struct Sys: Codable {
    let type : Int?
    let id : Int?
    let country : String?
    let sunrise : Int?
    let sunset : Int?
}

// MARK: - Weather
struct Weather: Codable {
    let id: Int?
    let main, description, icon: String?
}



